import sys
import os 
dir_path = os.path.dirname(os.path.realpath(__file__))
src_path = os.path.join(dir_path[:len(dir_path) - len('sandbox')], 'src')
sys.path.append(src_path)

import numpy as np
import rl_bot


# Fake action_space class

class ActionSpace(object):
    def sample(self):
        return np.array([0])


# Main

def main():
    bot = rl_bot.TestBot(ActionSpace(), 0.0)
    for i_episode in range(2000):
        # Reset bot and environment
        bot.new_game()

        # Play episode
        for t in range(20):
            # Action-Reward loop
            action = bot.action(random_observation())
            reward = (-1 * np.sum(action))
            bot.reward(reward)

        # Learn from experience
        bot.game_over()

        # Has the bot figured it out?
        bot_action = bot.action(random_observation(), True)
        output = np.sum(bot_action)

        if i_episode % 20 == 0:
            print ('\toutput {0}: {1}{2}'.format(str(i_episode).zfill(3), '-' if output < 0 else ' ', round(abs(output), 3)))

        if output < -100:
            print ('Bot has learned it! Policy gradients!')
            print ('proved w/ action: {0}'.format(bot_action))
            print ('Learned after {0} episodes'.format(i_episode + 1))
            break


def random_observation():
    # return np.array([1])
    return np.random.rand(2)


# Script handle

if __name__ == '__main__':
    main()