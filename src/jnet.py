# https://www.analyticsvidhya.com/blog/2016/04/neural-networks-python-theano/

import jlayer
import numpy as np


class JNet(object):

    # Constructor

    def __init__(self, test=False):
        self.test = test
        self.layers = []
        self.learning_rate = 0.01 if test else 0.001


    # Public methods

    def add_layer(self, layer_type, num_neurons):
        layer = None
        if layer_type == 'input' or layer_type == 'linear':
            layer = jlayer.IdentityLayer(num_neurons, self.test)
        elif layer_type == 'relu':
            layer = jlayer.ReLULayer(num_neurons, self.test)
        elif layer_type == 'leaky_relu':
            layer = jlayer.LeakyReLULayer(num_neurons, self.test)
        elif layer_type == 'tanh':
            layer = jlayer.TanHLayer(num_neurons, self.test)

        if len(self.layers) > 0:
            layer.set_previous(self.layers[-1])
            self.layers[-1].set_next(layer)
        self.layers.append(layer)


    def predict(self, input_vals):
        if len(self.layers) > 0:
            self.layers[0].forward(input_vals.astype(float))
            return self.layers[-1].get_output()
        return None


    def policy_gradients(self, input_vals, reward):
        cost = -1 * reward.astype(float)
        if len(self.layers) > 0:
            self.predict(input_vals)
            self.layers[-1].backward(cost)
            for layer in self.layers:
                layer.weight_update(self.learning_rate)


    # Getters

    def get_num_outputs(self):
        return self.layers[-1].get_size() if len(self.layers) > 0 else 0
        

    def __get_weights(self):
        weight_matrices = []
        for i in range(1, len(self.layers)):
            layer = self.layers[i]
            weight_matrices.append(layer._Layer__get_weights())
        return np.asarray(weight_matrices)


if __name__ == '__main__':
    print ('\nTesting to see if JNet will run')
    jnet = JNet(True)
    jnet.add_layer('input',      2)
    jnet.add_layer('linear',     4)
    jnet.add_layer('relu',       4)
    jnet.add_layer('leaky_relu', 4)
    jnet.add_layer('tanh',       2)

    test_input = np.array([1, 2])
    test_rewards = np.array([1, 2])

    prediction = jnet.predict(test_input)
    print ('Feedforward:\t\tcomplete without crash')

    jnet.policy_gradients(test_input, test_rewards)
    print ('Policy gradients:\tcomplete without crash')
    print ('')
