# Memory data container class

class Memory(object):

    def __init__(self, action, observation, reward=0):
        self.action = action
        self.observation = observation
        self.reward = reward


    def set_reward(self, reward):
        self.reward = reward