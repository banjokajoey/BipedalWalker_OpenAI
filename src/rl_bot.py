import brain
import jnet
import memory as mem
import numpy as np
import pickle


# Reinforcement Learning Bot agent class

class RLBot(object):

    # Constructor

    def __init__(self, action_space, explore_epsilon=1.0):
        self.brain = None
        self.memories = None
        self.action_space = action_space

        self.games_played = 0
        self.explore_epsilon = explore_epsilon
        self.explore_epsilon_decay = 0.9995
        self.last_observation = None


    # Interface

    def new_game(self):
        # Reset memory
        self.memories = []
        self.last_observation = None


    def action(self, observation, force_exploit=False):
        action = None
        if self.should_explore(force_exploit):
            # Explore
            action = self.action_space.sample()
        else:
            # Exploit
            td_observation = np.concatenate([self.last_observation, observation])
            action = self.brain.get_output(td_observation)

            # Remember
            memory = mem.Memory(action, td_observation)
            self.memories.append(memory)

        self.last_observation = observation
        return action


    def reward(self, reward):
        if len(self.memories) > 0:
            self.memories[-1].set_reward(reward)


    def game_over(self):
        # Increment game count
        self.games_played += 1

        # Bias toward exploitation
        self.explore_epsilon = max(0.1, self.explore_epsilon * self.explore_epsilon_decay)

        # Time to LEARN
        self.brain.learn(self.memories)


    def should_explore(self, force_exploit=False):
        return self.last_observation is None or (np.random.rand() < self.explore_epsilon and not force_exploit)



# Bipedal Walker Bot subclass

class BipedalBot(RLBot):

    # Constants 

    TD_SIZE = 2
    NUM_OBSERVATIONS = 24
    NUM_ACTIONS  = 4


    # Constructor

    def __init__(self, action_space, explore_epsilon=1.0):
        # Super init
        RLBot.__init__(self, action_space, explore_epsilon)

        # Convenience
        td_size = BipedalBot.TD_SIZE
        num_obs = BipedalBot.NUM_OBSERVATIONS
        num_act = BipedalBot.NUM_ACTIONS

        # Assemble bipedal bot brain
        model = jnet.JNet()
        model.add_layer('input',       td_size * num_obs)
        model.add_layer('leaky_relu',  200)
        model.add_layer('leaky_relu',  num_act)
        # model.add_layer('tanh',        num_act)
        self.brain = brain.BotBrain(model)


# Extremely simple test bot

class TestBot(RLBot):

    def __init__(self, action_space, explore_epsilon=1.0):
        # Super init
        RLBot.__init__(self, action_space, explore_epsilon)

        # Assemble brain
        model = jnet.JNet()
        model.add_layer('input', 4)
        model.add_layer('linear', 1)
        self.brain = brain.BotBrain(model)


 # Load/Save

def save_bot(bot, name):
    pickle.dump(bot, open(get_bot_filepath(name, bot.games_played), 'wb'))


def load_bot(name, index):
    return pickle.load(open(get_bot_filepath(name, bot.games_played), 'rb'))


def get_bot_filepath(name, index):
    return '../sandbox/saved_bots/{0}__bot{1}.p'.format(name, index)

