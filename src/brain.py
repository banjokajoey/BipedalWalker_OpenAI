# https://keon.io/deep-q-learning/
# http://edersantana.github.io/articles/keras_rl/
# http://karpathy.github.io/2016/05/31/rl/
# https://yanpanlau.github.io/2016/10/11/Torcs-Keras.html
# https://yanpanlau.github.io/2016/07/10/FlappyBird-Keras.html
# https://www.nervanasys.com/demystifying-deep-reinforcement-learning/


import numpy as np
import jnet
import memory


# Bot Brain

class BotBrain(object):

    def __init__(self, model=None):
        self.model = model
        self.reward_gamma = 0.9
        self.reward_gamma_cutoff = 0.001


    def get_output(self, input_vals):
        output = self.model.predict(np.array(input_vals))
        if np.isnan(np.min(output)):
            print ('\nCAUTION: brain is returning NaN {0}'.format(output))
            print ('input was: {0}'.format(input_vals))
        return output


    def learn(self, memories):
        # Get discounted future rewards
        rewards = self.discounted_future_rewards(memories)

        # Run policy gradients on all memories
        for i_memory, memory in enumerate(memories):
            observation = memory.observation
            reward = np.array([rewards[i_memory] for i in range(self.model.get_num_outputs())])
            self.model.policy_gradients(observation, reward)


    def discounted_future_rewards(self, memories):
        # Initialize all future rewards to 0
        discounted_rewards = [0.0] * len(memories)

        # For each memory of the previous game
        for i_memory, memory in enumerate(memories):
            # Accumulate future rewards
            discounted_reward = 0.0
            gamma = 1
            i_future = 0
            while i_memory + i_future < len(memories):
                # Calculate reward discount
                if gamma < self.reward_gamma_cutoff:
                    break

                # Calculate the relevance of this future reward
                future_mem = memories[i_memory + i_future]
                discounted_reward += gamma * future_mem.reward

                # Look further into the future
                i_future += 1
                gamma *= self.reward_gamma

            # Store future reward
            discounted_rewards[i_memory] = discounted_reward

        # Return results
        return discounted_rewards

