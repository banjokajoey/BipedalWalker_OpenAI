import gym
import numpy as np
import rl_bot


# Flags

RECORD_ENV = False
RENDER_ENV = True


# Constants

NUM_EPISODES = 2001


# Main

def main():
    # Initialize environemnt
    env = gym.make('BipedalWalker-v2')
    if RECORD_ENV:
        env = gym.wrappers.Monitor(env, '../videos/', force=True)

    # Initialize bipedal bot
    bot = rl_bot.BipedalBot(env.action_space)

    # Begin training
    print ('\nBegin bipedal training...')
    i_episode = 0
    while True:
        # Reset bot and environment
        bot.new_game()
        observation = env.reset()
        cumulative_reward = 0

        # Play episode
        t = 0
        while True:
            # Is this a status update?
            is_demo = should_render_env(i_episode)

            # Render environment
            if is_demo:
                env.render()

            # Action-Reward loop
            action = bot.action(np.asarray(observation), is_demo)
            observation, reward, done, info = env.step(action)
            bot.reward(reward)

            # Episode management
            t += 1
            cumulative_reward += reward
            if done:
                break

        # Report results.
        print ('\n\t{0}:\t{1} timesteps\t{2} reward\t{3} explore').format(i_episode, t, round(cumulative_reward, 2), round(bot.explore_epsilon, 2))
        
        # Learn from experience.
        bot.game_over()

        # Save bot
        if i_episode % 10000 == 0:
            rl_bot.save_bot(bot, 'basic')
        i_episode += 1

    print ('\nTraining complete\n')


# Helper

def reshape_env_observation(observation):
    return np.reshape(observation, 24)


def should_render_env(i_episode):
    return RENDER_ENV and i_episode % 1000 == 0 or i_episode == 500 or i_episode - 1 == NUM_EPISODES # i_episode in [i ** 3 for i in range(9)]


# Script handle

if __name__ == '__main__':
    main()