import numpy as np


# Neural Net Layer ABC

class Layer(object):

    def __init__(self, num_neurons, test=False):
        self.test = test
        self.size = num_neurons
        self.prev_layer = None
        self.next_layer = None
        self.in_acts = np.empty(num_neurons)
        self.out_acts = None
        self.weights = None
        self.weight_grads = None
        self.in_grads = None


    def set_previous(self, previous_layer):
        self.prev_layer = previous_layer
        weights_dim = (self.get_size(), previous_layer.get_size())
        if self.test:
            self.weights = np.ones(weights_dim)
            self.weight_grads = np.zeros(weights_dim)
        else:
            self.weights = np.random.rand(weights_dim[0], weights_dim[1])
            self.weight_grads = np.empty(weights_dim)


    def set_next(self, next_layer):
        self.next_layer = next_layer


    def get_size(self):
        return self.size


    def get_output(self):
        return self.out_acts


    def forward(self, vals):
        if self.prev_layer:
            vals = np.dot(self.weights, vals)

        self.in_acts = vals
        self.activation_fn()

        if self.next_layer:
            self.next_layer.forward(np.copy(self.out_acts))


    def backward(self, out_grads):
        if self.prev_layer:
            # Calculate in gradients
            self.activation_fn_drv(out_grads)

            # Calculate weight gradients
            in_grads_mat = np.asmatrix(self.in_grads).T
            prev_acts_mat = np.asmatrix(self.prev_layer.get_output())
            self.weight_grads = np.copy(in_grads_mat.dot(prev_acts_mat)) # TODO: had to copy for sum to work, why?

            # Backprop
            self.prev_layer.backward(np.sum(self.weight_grads, axis=0))


    def weight_update(self, learning_rate):
        if self.prev_layer:
            self.weights -= learning_rate * self.weight_grads


    def activation_fn(self):
        raise NotImplementedError()


    def activation_fn_drv(self, out_grads):
        raise NotImplementedError()


    # Private access, for unit testing

    def __get_weights(self):
        return self.weights


# Identity Layer

class IdentityLayer(Layer):

    def __init__(self, num_neurons, test=False):
        Layer.__init__(self, num_neurons, test)
        self.out_acts = np.empty(num_neurons)
        self.in_grads = np.empty(num_neurons)


    def activation_fn(self):
        # Do not transform the data.
        np.copyto(self.out_acts, self.in_acts)


    def activation_fn_drv(self, out_grads):
        # Derivative is 1, no change to gradients
        np.copyto(self.in_grads, out_grads)


# ReLU Layer

class ReLULayer(Layer):

    def __init__(self, num_neurons, test=False):
        Layer.__init__(self, num_neurons, test)


    def activation_fn(self):
        # Relu activation
        self.out_acts = np.copy(self.in_acts)
        self.out_acts[self.in_acts < 0] = 0


    def activation_fn_drv(self, out_grads):
        # Relu derivative
        self.in_grads = out_grads
        self.in_grads[self.in_acts < 0] = 0


# Leaky ReLU Layer

class LeakyReLULayer(Layer):

    def __init__(self, num_neurons, test=False):
        Layer.__init__(self, num_neurons, test)
        self.leak_factor = 0.05


    def activation_fn(self):
        # Leaky Relu activation
        self.out_acts = np.copy(self.in_acts)
        self.out_acts[self.in_acts < 0] *= self.leak_factor


    def activation_fn_drv(self, out_grads):
        # Leaky Relu derivative
        self.in_grads = out_grads
        self.in_grads[self.in_acts < 0] *= self.leak_factor


# TanH Layer

class TanHLayer(Layer):

    def __init__(self, num_neurons, test=False):
        Layer.__init__(self, num_neurons, test)


    def activation_fn(self):
        # TanH activation
        self.out_acts = np.tanh(self.in_acts)


    def activation_fn_drv(self, out_grads):
        # TanH derivative
        drv = np.negative(np.square(np.tanh(self.in_acts))) + 1
        self.in_grads = out_grads * drv

