import unittest
import numpy as np
from src import jnet


# JNet Policy Gradient Unit Tests

class TestJNetPolicyGradients(unittest.TestCase):

    # Test function

    def confirm_pg_performance(self, net, test_input, test_reward, expected_weights):
        net.policy_gradients(test_input, test_reward)
        weights = net._JNet__get_weights()
        all_close = np.allclose(weights, expected_weights, atol=1e-4)
        if not all_close:
            print ('expected: {0}'.format(expected_weights))
            print ('received: {0}'.format(weights))
        self.assertTrue(all_close)


    # Unit test setups

    def test_pg_linear(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('linear', 2)

        test_input = np.array([1, 2])
        test_reward = np.array([1, 0])
        expected_weights = [np.array([1.01, 1.02, 1, 1]).reshape(2, 2)]

        self.confirm_pg_performance(net, test_input, test_reward, expected_weights)


    def test_pg_chain(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('linear', 2)
        net.add_layer('linear', 2)

        test_input = np.array([1, 2])
        test_reward = np.array([1, 0])
        expected_weights = [np.array([1.03, 1.06, 1.03, 1.06]).reshape(2, 2), np.array([1.03, 1.03, 1, 1]).reshape(2, 2)]

        self.confirm_pg_performance(net, test_input, test_reward, expected_weights)


    def test_pg_layers_grow(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('linear', 3)

        test_input = np.array([1, 2])
        test_reward = np.array([0, 1, 0])
        expected_weights = [np.array([1, 1, 1.01, 1.02, 1, 1]).reshape(3, 2)]

        self.confirm_pg_performance(net, test_input, test_reward, expected_weights)


    def test_pg_layers_grow(self):
        net = jnet.JNet(True)
        net.add_layer('input', 3)
        net.add_layer('linear', 2)

        test_input = np.array([1, 2, 3])
        test_reward = np.array([1, 0])
        expected_weights = [np.array([1.01, 1.02, 1.03, 1, 1, 1]).reshape(2, 3)]

        self.confirm_pg_performance(net, test_input, test_reward, expected_weights)


    def test_pg_relu_1(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('relu', 2)

        test_input = np.array([-1, -2])
        test_reward = np.array([1, 0])
        expected_weights = [np.array([1, 1, 1, 1]).reshape(2, 2)]

        self.confirm_pg_performance(net, test_input, test_reward, expected_weights)


    def test_pg_relu_2(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('relu', 2)

        test_input = np.array([1, 2])
        test_reward = np.array([1, 0])
        expected_weights = [np.array([1.01, 1.02, 1, 1]).reshape(2, 2)]

        self.confirm_pg_performance(net, test_input, test_reward, expected_weights)


    def test_pg_leaky_relu(self):
        # TODO: finish this
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('leaky_relu', 2)

        test_input = np.array([-1, -2])
        test_reward = np.array([1, 0])
        expected_weights = [np.array([0.9995, 0.999, 1, 1]).reshape(2, 2)]

        self.confirm_pg_performance(net, test_input, test_reward, expected_weights)


    def test_pg_tanh(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('tanh', 2)

        test_input = np.array([0.5, 0.5])
        test_reward = np.array([1, 0])
        expected_weights = [np.array([1.00209987171, 1.00209987171, 1, 1]).reshape(2, 2)]

        self.confirm_pg_performance(net, test_input, test_reward, expected_weights)


# Main method

if __name__ == '__main__':
    unittest.main()