import unittest
from src import brain
from src import memory


# Reinforcment Learning Tests

class TestReinforcementLearning(unittest.TestCase):

    # Unit tests

    def test_discounted_future_rewards(self):
        false_memories = [
            memory.Memory(None, None, 0),
            memory.Memory(None, None, 0),
            memory.Memory(None, None, 1000),
            memory.Memory(None, None, 100),
            memory.Memory(None, None, 0),
            memory.Memory(None, None, 0)
        ]
        expected_rewards = [10.1, 101, 1010, 100, 0, 0]

        bot_brain = brain.BotBrain()
        bot_brain.reward_gamma = 0.1
        rewards = bot_brain.discounted_future_rewards(false_memories)

        for i, reward in enumerate(rewards):
            self.assertAlmostEqual(expected_rewards[i], reward)


# Main method

if __name__ == '__main__':
    unittest.main()