import unittest
import numpy as np
from src import jnet


# JNet Feedforward Unit Tests

class TestJNetFeedforward(unittest.TestCase):

    # Test function

    def confirm_ff_performance(self, net, test_input, expected_output):
        net_output = net.predict(test_input)
        all_close = np.allclose(expected_output, net_output)
        if not all_close:
            print ('expected: {0}'.format(expected_output))
            print ('received: {0}'.format(net_output))
        self.assertTrue(all_close)


    # Unit test setup

    def test_ff_input_layer(self):
        net = jnet.JNet()
        net.add_layer('input', 2)    

        test_input = np.array([1, 2])
        expected_output = np.copy(test_input)

        self.confirm_ff_performance(net, test_input, expected_output)


    def test_ff_chain(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('linear', 2)
        net.add_layer('linear', 2)

        test_input = np.array([1, 2])
        expected_output = np.array([6, 6])

        self.confirm_ff_performance(net, test_input, expected_output)


    def test_ff_layers_grow(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('linear', 3)

        test_input = np.array([1, 2])
        expected_output = np.array([3, 3, 3])

        self.confirm_ff_performance(net, test_input, expected_output)


    def test_ff_layers_shrink(self):
        net = jnet.JNet(True)
        net.add_layer('input', 3)
        net.add_layer('linear', 2)

        test_input = np.array([1, 2, 3])
        expected_output = np.array([6, 6])

        self.confirm_ff_performance(net, test_input, expected_output)


    def test_ff_relu(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('relu', 2)

        test_input = np.array([-2, -2])
        expected_output = np.array([0, 0])

        self.confirm_ff_performance(net, test_input, expected_output)


    def test_ff_leaky_relu(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('leaky_relu', 2)

        test_input = np.array([-2, -2])
        expected_output = np.array([-0.2, -0.2])

        self.confirm_ff_performance(net, test_input, expected_output)


    def test_ff_tanh(self):
        net = jnet.JNet(True)
        net.add_layer('input', 2)
        net.add_layer('tanh', 2)

        test_input = np.array([1.1] * 2)
        expected_output = np.array([0.975743] * 2)

        self.confirm_ff_performance(net, test_input, expected_output)


# Main method

if __name__ == '__main__':
    unittest.main()